import java.util.Arrays;
import java.util.LinkedList;

public class Grafo {

    private Nodo[] listaNodos = null;
    private int matrizAdya[][]  = null;//sera una matriz cuadratica

    private int numNodos = 0;
    private int numNodosIntro = 0;

    private int[] marcados;
    private int orden = 1;


    public Nodo[] recorrerPriomeroProfundidad() {
        Nodo[] lista= new Nodo[numNodos];
        Arrays.fill(marcados, 0);//pongo todo a cero para empezar

        for (int i = 0; i < numNodos; i++) {
            if(marcados[i] == 0)
                rpp(i);
        }

        orden = 1;

        for (int i = 0; i < marcados.length; i++) {
            lista[marcados[i]-1] = listaNodos[i];
        }
        return lista;
    }

    private void rpp(int indice){
        marcados[indice] = orden++;
        int w;
        for (w = 0; w < numNodos; w++) {
            if((matrizAdya[indice][w] != 0) && marcados[w] == 0){
                rpp(w);
            }
        }
    }

    /**
     * borra todas las marcas de los nodos
     * para todo nodo del grafo
     *    si (el nodo no esta marcado(
     *      rpa(nodo(
     */

    public Nodo[] recorrePrimeroAnchura(){
        Nodo[] lista= new Nodo[numNodos];

        Arrays.fill(marcados, 0);

        orden = 1;

        for (int i = 0; i < numNodos; i++) {
            if(marcados[i] == 0)
                rpa(i);

        }
        for (int i = 0; i < marcados.length; i++) {
            lista[marcados[i]-1] = listaNodos[i];
        }
        return lista;

    }

    private void rpa(int indice) {
        LinkedList<Integer> cola = new LinkedList<>();
        marcados[indice] = orden++;

        cola.addLast(indice);
        while(!cola.isEmpty()){
            int x = cola.getFirst();
            cola.removeFirst();
            int w;
            for (w = 0; w < numNodos; w++) {
                if((matrizAdya[indice][w] != 0) && marcados[w] == 0){
                   marcados[w]=orden++;
                   cola.addLast(w);
                }
            }
        }

    }


    public Grafo(int numNodos){
        this.numNodos=numNodos;
        matrizAdya = new int[numNodos][numNodos];
        listaNodos = new Nodo[numNodos];

        for (int i = 0; i < matrizAdya[1].length; i++) {
            Arrays.fill(matrizAdya[i],0);
        }
        marcados= new int[numNodos];

    }
   public boolean addNodo(Nodo nodo){
        if(this.numNodosIntro == this.numNodos)
            return false;
        if( positionNodo(nodo)!= -1) //verificar que no este ya insertado ese nodo
            return false;
        listaNodos[numNodosIntro++]=nodo;
        return true;
   }
    public boolean addArista(Arista arista){
        int n1 = positionNodo(arista.getN1());
        int n2 = positionNodo(arista.getN2());
        int peso = arista.getPeso();

        if((n1 < 0) || (n2 < 0)|| (peso <= 0))
            return false;

        matrizAdya[n1][n2] = peso;
        return true;

    }
    public boolean removeNodo(Nodo nodo){
        int pos = positionNodo(nodo);

        if(pos < 0) return false;

        for (int i = pos + 1; i < numNodosIntro ; i++) {
            for (int j = 0; j < numNodosIntro; j++) {
                matrizAdya[i-1][j] = matrizAdya[i][j];
            }
        }

        for (int i = 0; i <  numNodosIntro ; i++) {
            for (int j = pos + 1; j < numNodosIntro; j++) {
                matrizAdya[i][j-1] = matrizAdya[i][j];
            }
        }

        for (int i = pos + 1; i < numNodosIntro; i++) {
            listaNodos[i-1] = listaNodos[i];
        }
        numNodosIntro--;
        return true;
    }
    public boolean removeArista(Arista arista){
        int n1 = positionNodo(arista.getN1());
        int n2 = positionNodo(arista.getN2());
        if((n1 < 0) || (n2 < 0) || matrizAdya[n1][n2]==0)
            return false;
        matrizAdya[n1][n2]=0;
        return true;
    }
    public Nodo[] getNodos(){
        Nodo[] nodos = new Nodo[numNodosIntro];//solo mandamos lo que estar ya cargado, no celdas vacias
        for (int i = 0; i < numNodosIntro; i++) {
            nodos[i] = listaNodos[i];
        }
        return nodos;
    }
    public Arista[] getAristas( ){
        int numAristas = 0;
        Arista[] listaAristas =  new Arista[numAristas()];
        for (int i = 0; i < numNodosIntro; i++) {
            for (int j = 0; j < numNodosIntro; j++) {
                if(matrizAdya[i][j] >0)
                    listaAristas[numAristas++] = new Arista(listaNodos[i],listaNodos[j],matrizAdya[i][j]);

            }
        }
        return listaAristas;
    }

    public int positionNodo(Nodo nodo) {
        for (int i = 0; i < numNodosIntro ; i++) {
            if(listaNodos[i].igual(nodo))
                return i;
        }
        return -1;
    }

    public int numAristas(){
        int num = 0;
        for (int i = 0; i < numNodosIntro; i++) {
            for (int j = 0; j < numNodosIntro; j++) {
                if(matrizAdya[i][j] >0)
                    num++;
            }
        }
        return num;
    }


    public String toString() {
        String matriz ="";
        for (int i = 0; i < numNodosIntro; i++) {
            for (int j = 0; j < numNodosIntro; j++) {
                matriz += "["+matrizAdya[i][j]+"]" ;
            }
            matriz += "\n";
        }
        return "Grafo{" +
                "\nlistaNodos=" + Arrays.toString(listaNodos) +
                ", \nmatrizAdya=\n" + matriz +
                '}';
    }

    public static void main(String[] args) {
        Grafo g = new Grafo(6);
        Nodo n1 = new Nodo("1");
        Nodo n2 = new Nodo("2");
        Nodo n3 = new Nodo("3");
        Nodo n4 = new Nodo("4");
        Nodo n5 = new Nodo("5");
        g.addNodo(n1);
        g.addNodo(n2);
        g.addNodo(n3);
        g.addNodo(n4);
        g.addNodo(n5);
        g.addArista(new Arista(n1,n2,4));
        g.addArista(new Arista(n1,n5,1));
        g.addArista(new Arista(n3,n2,6));
        g.addArista(new Arista(n5,n5,3));
        g.addArista(new Arista(n4,n1,9));
        System.out.println(g);
        g.removeNodo(n4);
        System.out.println(g);
        g.removeArista(new Arista(n3,n2,6));
        System.out.println(g);
        System.out.println(g.recorrePrimeroAnchura());



    }

}
