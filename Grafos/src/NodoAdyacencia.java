public class NodoAdyacencia {

    int peso;
    Nodo nodo;

    public NodoAdyacencia(int peso) {
        this.peso = peso;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public Nodo getNodo() {
        return nodo;
    }

    public void setNodo(Nodo nodo) {
        this.nodo = nodo;
    }

    public NodoAdyacencia(int peso, Nodo nodo) {
        this.peso = peso;
        this.nodo = nodo;
    }

    @Override
    public String toString() {
        return "NodoAdyacencia{" +
                "peso=" + peso +
                ", nodo=" + nodo +
                '}';
    }
}
