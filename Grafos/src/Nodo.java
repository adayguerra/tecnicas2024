

public class Nodo {
    private String nombre;

    public Nodo(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return this.nombre;
    }

    public boolean igual(Nodo v){
        return v.getNombre().equals(this.nombre);
    }

    @Override
    public String toString() {
        return "Nodo{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}


