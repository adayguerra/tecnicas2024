import java.util.ArrayList;
import java.util.Arrays;

public class GrafoLista {
    private Nodo[] listaNodos = null;
    private ArrayList<Nodo>[] listaAdyacencia = null;

    private int numNodos;
    private int numeroNodoIntro = 0;

    public GrafoLista(int numNodos){
        this.numeroNodoIntro = 0;
        this.numNodos = numNodos;
        listaAdyacencia = new ArrayList[numNodos];
        listaNodos = new Nodo[numNodos];
    }

    public boolean addNodo(Nodo n){
         if(this.numeroNodoIntro == numNodos)
             return false;

         if(this.nodePosition(n) != -1)
             return false;

        listaAdyacencia[numeroNodoIntro] = new ArrayList<>();
        listaNodos[numeroNodoIntro++] = n;
        return true;
    }
    public boolean addArista(Arista arista){
        int n1 = nodePosition(arista.getN1()); //ya tengo las posiciones de el nodo 1 y el nodo 2 en el array lista nodos
        int n2 = nodePosition(arista.getN2());


        if(n1 < 0 || n2 < 0 )
            return false;

        ArrayList<Nodo> aux  = listaAdyacencia[n1];

        for(Nodo n:aux)
            if(n.igual(arista.getN2()))
                return false;

        listaAdyacencia[n1].add(arista.getN2());
        return true;
    }
    public int nodePosition(Nodo n){
        for (int i = 0; i < numNodos; i++) {
            if(listaNodos[i] != null)
                if(listaNodos[i].igual(n))
                    return i;
        }
        return -1;
    }
    public boolean deleteNode(Nodo n){
        int pos = nodePosition(n);

        if(pos < 0)
            return false;

        for (int i = 0; i < numeroNodoIntro ; i++) {
            ArrayList<Nodo> listaAux = listaAdyacencia[i];
            Nodo auxNode = null;
            for (Nodo nodolistaaux : listaAux)
                if (nodolistaaux.igual(n))
                    auxNode = nodolistaaux;

            if (auxNode != null)
                listaAdyacencia[i].remove(auxNode);
        }
        for (int i = pos+1; i < numeroNodoIntro; i++) {
            listaAdyacencia[i-1] = listaAdyacencia[i];
        }
        for (int i = pos+1; i < numeroNodoIntro; i++) {
            listaNodos[i-1] = listaNodos[i];
        }
        numeroNodoIntro--;
        return true;

    }
    public boolean deleteArista(Arista arista){
        int n1 = nodePosition(arista.getN1()); //ya tengo las posiciones de el nodo 1 y el nodo 2 en el array lista nodos
        int n2 = nodePosition(arista.getN2());


        if(n1 < 0 || n2 < 0 )
            return false;

        ArrayList<Nodo> aux = listaAdyacencia[n1];
        for(Nodo n:aux)
            if(n.igual(arista.getN2())) {
                listaAdyacencia[n1].remove(arista.getN2());
                    return true;
            }
        return false;
    }
    public static void main(String[] args) {


        GrafoLista g = new GrafoLista(5);

        Nodo n1 = new Nodo("1");
        Nodo n2 = new Nodo("2");
        Nodo n3 = new Nodo("3");
        Nodo n4 = new Nodo("4");
        Nodo n5 = new Nodo("5");
        g.addNodo(n1);
        g.addNodo(n2);
        g.addNodo(n3);
        g.addNodo(n4);
        g.addNodo(n5);
        g.addArista(new Arista(n1,n2,4));
        g.addArista(new Arista(n1,n5,1));
        g.addArista(new Arista(n3,n2,6));
        g.addArista(new Arista(n5,n5,3));
        g.addArista(new Arista(n4,n1,9));
        System.out.println(g);
        g.deleteNode(n4);
        System.out.println(g);
        g.deleteArista(new Arista(n3,n2,6));
        System.out.println(g);
    }
    public String toString() {

        return "Grafo{" +
                "\nlistaNodos=" + Arrays.toString(listaNodos) +
                ", \nlistaadaya=\n" + Arrays.toString(listaAdyacencia) +
                '}';
    }



}
