public class Arista {
    private Nodo n1;
    private Nodo n2;

    private int peso;

    public Arista(Nodo n1, Nodo n2){
        this.n1 = n1;
        this.n2 = n2;
        this.peso = -1;
    }

    public Arista(Nodo n1, Nodo n2, int peso){
        this.n1 = n1;
        this.n2 = n2;
        this.peso = peso;
    }

    public Nodo getN1() {
        return n1;
    }

    public Nodo getN2() {
        return n2;
    }

    public int getPeso() {
        return peso;
    }

    @Override
    public String toString() {
        String aux= "Arista{ n1=" + n1 + ", n2=" + n2;
        if(peso!=-1) aux+= ", peso=" + peso;
        return aux + '}';
    }
}
