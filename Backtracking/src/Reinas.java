import java.util.Arrays;

public class Reinas {

    private static int[] reinas(int reinas){
        int sol[] = new int[reinas]; // para 8 reinas será = 8
        int nivel = 0;
        boolean fin = false;
        Arrays.fill(sol,-1);
        int k = 0;
        do{
            sol[nivel]++;
            if( nivel == (reinas-1) && noEstanEnJaque(nivel,sol)){
                fin = true;
            }else if(noEstanEnJaque(nivel,sol)){
                nivel++;
                sol[nivel] = -1;
                System.out.println("Paso por aquí."+(k++));
            }else{
                while(sol[nivel] == (reinas -1)){
                    sol[nivel] = -1;
                    nivel--;
                }
            }
        }while(!fin);
        return sol;

    }

    private static boolean noEstanEnJaque(int nivel, int[] sol) {
        for(int i = 0; i < nivel; i++){
            if(sol[i] == sol[nivel]){
                return false;
            }else if(Math.abs(nivel - i) == Math.abs(sol[nivel] - sol[i])){
                return false;
            }
        }  return true;
    }


    //BONOLOTO

    public static int[] bonoloto( int k){
        int sol[] = new int[6];
        int num = 1;
        int nivel = 0;

        Arrays.fill(sol,0);
        do{
            sol[nivel]++;
            if( nivel == 5 && (suma(nivel, sol) == k)){
                return sol;
            }else if(nivel < 5 && (suma(nivel, sol) < k)){
                sol[nivel+1] = sol[nivel];
                nivel++;
            }else{
                while(nivel >= 0 && (sol[nivel] >= 44+nivel)){
                    nivel--;
                }
            }

        }while(nivel >= 0);
        return sol;
    }

    private static int suma(int nivel, int[] sol) {
        int sum = 0;
        for(int i = 0; i <= nivel; i++){
            sum += sol[i];
        }
        return sum;
    }


    public static void main(String[] args) {
        System.out.println(Arrays.toString(reinas(8)));


        System.out.println(Arrays.toString(bonoloto(260)));
    }




}
