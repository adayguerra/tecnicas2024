import org.w3c.dom.ls.LSOutput;

public class TorreHanoi {
    public static void torreHanoi(int numero, String origen, String destino, String auxiliar) {
        if(numero == 1)
            System.out.println("Muevo uno de "+origen+ " a "+ destino);
        else {

            torreHanoi(numero-1, origen, auxiliar, destino);

            torreHanoi(1, origen,  destino, auxiliar);

            torreHanoi(numero-1,  auxiliar, destino, origen);
        }
    }

    public static void main(String[] args) {
        int numDiscos = 3;
        torreHanoi(numDiscos, "A", "B", "C");
    }
}
