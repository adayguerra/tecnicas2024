package ejercicio2;


import java.awt.image.AreaAveragingScaleFilter;
import java.util.Arrays;

/*
⎧ 1, 𝑛 = 0
⎪ 𝑛∗𝑓(𝑛−1),0<𝑛<3 𝑓(𝑛)= ⎨2𝑛∗𝑓(𝑛−2),6>𝑛≥3
⎪3𝑛 ∗ 𝑓(𝑛 − 3), 10 ≥ 𝑛 ≥ 6 ⎩ 4𝑛∗𝑓(𝑛−4),𝑛>10
 */
public class ejercicio2 {

    public static int funciondinamica(int n){
        int resultados[] = new int[n+1];
        Arrays.fill(resultados,-1);
        resultados[0] = 1;
        funciondinamica(resultados, n);
        System.out.println(Arrays.toString(resultados));
        return resultados[n];
    }

    public static int funciondinamica (int resultados[], int n){

        if(resultados[n] != -1){
            return resultados[n];
        }

        int result = -1;

        if(n == 0)
             result = 1;
        else if ( 0 < n && n < 3)
            result =  n*funciondinamica(resultados, n-1);
        else if ( 6 > n && n >= 3)
            result =  2*n*funciondinamica(resultados, n-2);
        else if ( 10 >= n && n >= 6)
            result =  3*n*funciondinamica(resultados, n-3);
        else if ( n > 10)
            result =  4*n*funciondinamica(resultados, n-4);

        resultados[n] = result;
        return result;


    }


//[1, 1, 2, -1, 16, -1, -1, 336, -1, -1, -1, 14784, -1, -1, -1, 887040]
    public static void main(String[] args) {
        System.out.println("resultado posicion 15: "+funciondinamica(14));

    }
}
