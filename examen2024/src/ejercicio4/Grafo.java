package ejercicio4;

class Grafo {
    public Grafo(int NumVertices) {
    }

    public void añadirArista(Arista arista) throws GrafoException {
    }

    public void añadirVertice(Vertice ver) throws GrafoException {
    }

    public void eliminarArista(Arista arista) throws GrafoException {
    }

    public void eliminarVertice(Vertice ver) throws GrafoException {
    }

    private int posicionVertice(Vertice ver) {
    }

    public Vertice[] listaVertices() {
    }

    public Arista[] listaAristas() {
    }

    private int numeroDeAristas() {
    }

    private int numeroDeVertices() {
    }

    public int GradoDivisible3(){
        int numverticesdisibles = 0;
        Vertice vertices[] = listaVertices();
        Vertice temp;

        for (int i = 0; i < vertices.length ; i++) {
            temp = vertices[i];
            int peso = contarAristas(temp);
            if( peso%3 == 0)
                numverticesdisibles++;


        }
        return numverticesdisibles;

    }

    public int contarAristas(Vertice ver){
        Arista []aristas = listaAristas();
        int contador = 0;
        for (int i = 0; i < aristas.length; i++) {
            if(aristas[i].getV1().igual(ver))
                contador++;
            if(aristas[i].getV2().igual(ver))
                contador++;

        }
        return contador;
    }
}
class Vertice {
    private String nombre;
    public Vertice(String nombre) {}
    public String getNombre() {}
    public boolean igual(Vertice v) {}
}
class Arista {
    private Vertice v1;
    private Vertice v2;
    private int peso;
    public Arista(Vertice v1, Vertice v2, int peso) {}
    public Vertice getV1() {}
    public Vertice getV2() {}
    public int getPeso() {}
}


/**
 * Pruebas 1:
 * Objetivo: Test de uso normal del calculo del nodo del grafo divisible entre 3
 * Preambulo: Crear un grafo con los veritices y las aritas como minimo con más de unos que tenga 3 aristas asociadas
 * Cuerpo: calcula  si es tiene nodos divisibles entre 3
 * Postambulo: nada
 * Resultados esperados: el numero de veritices que tenía un numero de aritas divisible en 3.
 */

/**
 * Pruebas 2:
 * Objetivo: Test de uso normal del calculo del nodo del grafo divisible entre 3
 * Preambulo: Crear un grafo con los veritices y las aritas como minimo con más de unos que tenga 3 aristas asociadas
 * Cuerpo: calcula  si es tiene nodos divisibles entre 3
 * Postambulo: nada
 * Resultados esperados: el numero de veritices que tenía un numero de aritas divisible en 3.
 */