import java.util.Arrays;

public class AlgoritmosAvidos {
    // ALGORITMO DE MONEDA O DEVOLUCI'ON DE CAMBIO
    public static int[] devolverCambio(int sistemaMoneda[], int P){
        int valorActual = 0;
        int resultado[] = new int[sistemaMoneda.length];//[2e][1e][50c][20c][10c][5c][2c][1c] P= 10,54e =>[0][2][0][0][0][1][0][5]

        while(valorActual != P){
            int proximaMoneda = elegirMoneda(sistemaMoneda, P - valorActual);
            if(proximaMoneda == -1){
                System.out.println("No hay soluci'on");
                return null;
            }
            resultado[proximaMoneda]++;
            valorActual += sistemaMoneda[proximaMoneda];

        }
        return resultado;
    }

    private static int elegirMoneda(int[] sistemaMoneda, int P) {
        int indice = 0;
        for(int j = 1; j < sistemaMoneda.length; j++){
            if((sistemaMoneda[j] > sistemaMoneda[indice]) && (sistemaMoneda[j] <= P)){
                indice = j;
            }
        }
        if(sistemaMoneda[indice] <= P)
            return indice;
        else return -1;
    }

    public static double[] calculoMochila(int tamanoMochila, int []pesos, int[]beneficios){
        int pesoActual = 0;
        boolean elementoUsado[] = new boolean[pesos.length];
        double solucion[] = new double[pesos.length];

        Arrays.fill(elementoUsado, false);

        while(pesoActual < tamanoMochila){
            int elementoSeleccionado = elegirObjeto(pesos,beneficios,elementoUsado);

            if(elementoSeleccionado == -1){
                break;
            }

            elementoUsado[elementoSeleccionado] = true;
            if(pesoActual + pesos[elementoSeleccionado] <= tamanoMochila){
                solucion[elementoSeleccionado] = 1;
                pesoActual += pesos[elementoSeleccionado];
            }else{
                solucion[elementoSeleccionado] = (double) (tamanoMochila - pesoActual)/pesos[elementoSeleccionado];
                pesoActual = tamanoMochila;
            }


        } return solucion;
    }
    public static int elegirObjeto(int[] pesos,int[] beneficios, boolean[] elementoUsado){
        int indice = 0;
        while(indice < pesos.length && elementoUsado[indice]){
            indice++;
        }

        for(int i = indice+1; i < pesos.length; i++){
            if(!elementoUsado[i]  && ( (double)beneficios[i]/pesos[i] >  (double)beneficios[indice]/pesos[indice])){
                indice = i;
            }
        }
        if(indice >= pesos.length){
            return -1;
        }
        return indice;
    }














    public static void main(String[] args) {
        int sistemaMontenario[] = {1,2,5,10,20,50,100,200};

        double cantidadD = 5.89;
        int cantidad = (int) (100.00*cantidadD);

        System.out.println("P="+cantidad);
        System.out.println(Arrays.toString(sistemaMontenario));
        System.out.println(Arrays.toString(devolverCambio(sistemaMontenario, cantidad)));



        int tamanoMochia = 20;
        int []pesos  = {18,15,10};
        int []beneficios = {25,24,15};

        System.out.println("Mochila");
        System.out.println(Arrays.toString(calculoMochila(tamanoMochia, pesos, beneficios)));
    }
}
