import java.util.Arrays;

public class ProgramacionDinamica {

    // n = 1, 0 => 1
    // n > 1, f(n-1) + f(n-2)

    public static long fibonacci(int n) {

        if(n == 1 || n == 0) {
            return 1;
        }else{
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    public static long fibonaccinDinamico(int n) {
        long []tabla = new long [n + 1];
        Arrays.fill(tabla,0);

        return fibonaccinDinamico(n, tabla);
    }
    public static long fibonaccinDinamico(int n, long[] tabla) {
        if(tabla[n] > 0) {
            return tabla[n];
        }

        if(n == 0 || n == 1) {
            tabla[n] = 1;
        }else{

            tabla[n] = fibonaccinDinamico(n - 1, tabla) + fibonaccinDinamico(n - 2, tabla);
        }
        return tabla[n];
    }



    public static boolean[] mochilaDinamico(int p[], int b[], int T){
        int beneficio[][] = rellenaTabla(p,b,T);

        boolean sol[] = new boolean[p.length];
        int i  = p.length;
        int j = T;
        while(j > 0){

            if( beneficio[i][j] == beneficio[i-1][j])
                i--;
            else {
                i--;
                sol[i] = true;
                j = j - p[i];
            }

        }
        System.out.println(Arrays.toString(beneficio));
        return sol;
    }

    private static int[][] rellenaTabla(int[] p, int[] b, int t) {
        int beneficio[][] = new int[p.length+1][t+1];
        for (int i = 0; i < p.length ; i++) {
            beneficio[0][i] = 0;
        }
        System.out.println(Arrays.toString(beneficio));
        for (int i = 1; i <= p.length; i++) {
           for(int j = 0; j < p[i-1] ; j++){
               beneficio[i][j] = beneficio[i-1][j];
           }

            for (int j = p[i-1]; j <= t ; j++) {
                beneficio[i][j] = Math.max( beneficio[i-1][j], b[i-1] + beneficio[i-1][j-p[i-1]]);

            }
        }return beneficio;

    }

    public static void main(String[] args) {
        long startTime = System.nanoTime();
        fibonacci(30);
        long endTime = System.nanoTime();
        long elapsedTime = endTime - startTime;
        System.out.println("time:"+elapsedTime);

        long startTime2 = System.nanoTime();
        fibonaccinDinamico(30);
        long endTime2 = System.nanoTime();
       long elapsedTime2 = endTime2 - startTime2;
        System.out.println("time dinamico:"+elapsedTime2);

        int []p = {2,3,4};
        int []b = {1,2,5};
        int T = 6;

        System.out.println(Arrays.toString(mochilaDinamico(p,b,T)));

    }
}


