package org.example.jsp_el;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JspElApplication {

    public static void main(String[] args) {
        SpringApplication.run(JspElApplication.class, args);
    }

}
