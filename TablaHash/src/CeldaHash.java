public class CeldaHash <E,K>{
    private E dato;
    private K clave;
    public boolean deleted;

    public CeldaHash(E dato, K clave) {
        this.dato = dato;
        this.clave = clave;
    }

    public E getDato() {
        return dato;
    }

    public K getClave() {
        return clave;
    }

    public boolean isDeleted(){
        return deleted;
    }

    public void setDeleted(boolean deleted){
        this.deleted = deleted;
    }
}
