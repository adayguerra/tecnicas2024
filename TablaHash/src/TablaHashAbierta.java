import java.util.ArrayList;

public class TablaHashAbierta<E,K> {

    ArrayList <CeldaHash<E,K>>[]tabla;
    private int numeroElementos;
    private final int FACTOR_CARGA = 100;


    public boolean insertar(E dato, K clave){
        if(dato == null  || clave == null)
            return false;

        if(buscar(clave)!=null)
            return false;

        if( ( numeroElementos + 1 ) * 100 / tabla.length > FACTOR_CARGA)
            rehash();

        int pos = fHash(clave);
        if(tabla[pos] == null)
            tabla[pos] = new ArrayList<>();

        tabla[pos].add(new CeldaHash(dato,clave));
        numeroElementos++;
        return  true;

    }

    private int fHash(K clave) {

        return (int)clave % tabla.length;
    }

    public E buscar(K clave){
        if(clave == null)
            return null;

        int pos = fHash(clave);

        for( CeldaHash<E,K> celda:tabla[pos])
            if(celda.getClave().equals(clave))
                return celda.getDato();

        return null;

    }

    public  boolean borrar(K clave){
        if(clave == null)
            return false;
        int pos = fHash(clave);

        if(tabla[pos] == null)
            return false;

        for( CeldaHash<E,K> celda:tabla[pos])
            if(celda.getClave().equals(clave))
                if(tabla[pos].remove(celda))
                    return true;


        return false;


    }

    public void rehash(){
        ArrayList<CeldaHash<E,K>> tablaAntigua[] = tabla;

        tabla = new ArrayList[tabla.length*2+1];
        numeroElementos =0;

        for( ArrayList<CeldaHash<E,K>> array:tablaAntigua)
            if(array != null)
                for( CeldaHash<E,K> celda:array)
                    insertar(celda.getDato(), celda.getClave());

    }



}
