import java.util.ArrayList;

public class TablaHash <E,K>{
   
    private CeldaHash<E,K>[] tabla;//La Tabla Hash
    private int numElementos;
    private final int FACTOR_CARGA = 50;//50% Si se supera se tiene que hacer rehash

    public TablaHash() {
        this.tabla = new CeldaHash[11];
    }

    public TablaHash(int tamano) {
        this.tabla = new CeldaHash[(tamano<11)?11:tamano];//comprobación ternaria si es menor que 11 ponermos ese minimo
    }

    /**
     * Insertar un elemento con su clave en la tabla hash
     * @param dato El dato que se quiere guardar
     * @param clave La clave del dato a guardar
     * @return true si se ha insertado correctamente y false en caso contrario
     */
    public boolean insertar(E dato, K clave){
        if(dato == null || clave == null)
            return false;

        if((numElementos + 1)*100/tabla.length > FACTOR_CARGA)// ((5+1)*100)/10 > 50 -> rehash()
            rehash();

        int hash = fHash(clave);//función hash que se encarga de obtener la posición a guardar
        int pos = hash;

        int colision = 0;

        while(tabla[pos]!= null && !tabla[pos].isDeleted()){//mientras que la posición no esté vacía ocurre una colisión
            if(tabla[pos].getClave().equals(clave))//verificamos que la posición no tenga ya ese mismo valor.
                return false;//si lo tiene no hace falta insertar y salimos
            colision++;//en cambio, si es diferente hemos tenido una colisión
            pos = resolucionLineal(clave, colision);//resolución de la colisión resolución lineal. me da una nueva posición
            if(colision>tabla.length) {//si he tenido más colisiones que el tamaño de la tabla es que ya no se puede insertar
                return false;
            }
        }
        tabla[pos] = new CeldaHash<>(dato, clave);//hemos encontrado una posición vacia y podemos guardar nuestro celdahash dato
        numElementos++;
        return true; //terminamos
    }

    private void rehash() {
        CeldaHash<E,K> tablaAntigua[] = tabla;
        tabla = new CeldaHash[tabla.length*2+1];

        numElementos = 0;
        for(CeldaHash<E,K> celda: tablaAntigua)
            if(celda!= null && !celda.isDeleted() )
                insertar(celda.getDato(),celda.getClave());

    }

    public int resolucionLineal(K clave, int colision){
        return (fHash(clave) + colision) % tabla.length;
    }

    public int resolucionCuadratica(K clave, int colision){
        return (fHash(clave) + colision*colision) % tabla.length;
    }

    public int resolucionDobleDispersion(K clave, int colision){
        return (fHash(clave) + colision * fHash2(clave)) % tabla.length;
    }
    private int fHash(K clave) {

        return (int)clave % tabla.length;
    }
    private int fHash2(K clave) {
        return 0;
    }

    /**
     * Busca el dato que correpsonde con la clave dada
     * @param clave La clave del dato que se quiere recurar
     * @return El elemento de la tabla que tiene la clave buscada. Si no existe devuelve null
     */
    public E buscar(K clave){

        if(clave == null){
            return null;
        }

        int hash = fHash(clave);
        int pos = hash;

        int colision = 0;
        while(tabla[pos]!=null){
            if(!tabla[pos].isDeleted() && tabla[pos].getClave().equals(clave)){
                return tabla[pos].getDato();
            }
            colision++;
            pos = resolucionLineal(clave,colision);
        }

        return null;
    }







    /**
     * Elimina de la tabla el dato que corresponde con la clve
     * @param clave La clave del dato que se desea eliminar
     * @return
     */
    public boolean borrar(K clave){
        if(clave == null)
            return false;


        int pos =  fHash(clave);;
        int colision = 0;
        while(tabla[pos]!= null){
            if(!tabla[pos].isDeleted() && tabla[pos].getClave().equals(clave)){
                tabla[pos].setDeleted(true);
                return true;
            }
            colision++;
            pos = resolucionLineal(clave,colision);

        }
        return false;
    }

    private int fHash(int clave){
        return clave*clave % tabla.length;
    }

    private int fHash(String clave){
        int hash = 0;
        for(char c:clave.toCharArray()){
            hash += c;
        }
        return hash % tabla.length;
    }

    private int fHash1(int clave){
        return clave % tabla.length;
    }
}
/**
 * TablaHash <Persona, String> miTablaHash
 */