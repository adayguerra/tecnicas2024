import java.util.Arrays;
import java.util.Random;

/**
 * 
 */
public class DemoTamanoTabla {
    public static void main(String[] args) {
        Random random = new Random();
           
        int [] valores = random.ints(11, 1,100).toArray();

        for(int i = 0; i < 100; i++){
            System.out.print (valores[i % valores.length] + " ");
            if(i%11==0)
                System.out.println("");
        }
    }
}
